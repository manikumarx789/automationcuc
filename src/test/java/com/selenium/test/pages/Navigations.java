package com.selenium.test.pages;

import com.selenium.framework.base.SeleniumBase;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("cucumber-glue")
public class Navigations extends SeleniumBase {
    @FindBy(xpath = "//*[@id='nav-xshop']/a[2]")
    public static WebElement DEAL;
    @FindBy(xpath = "//*[@id='nav-xshop']/a[3]")
    public static WebElement PAY;
    @FindBy(xpath = "//*[@id='nav-xshop']/a[4]")
    public static WebElement SELL;
    @FindBy(xpath = "//*[@id='nav-xshop']/a[5]")
    public static WebElement CUSTOMER;

    public Navigations(WebDriver driver) {
        super(driver);
    }
}
