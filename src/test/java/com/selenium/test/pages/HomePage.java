package com.selenium.test.pages;

import com.selenium.framework.base.SeleniumBase;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("cucumber-glue")
public class HomePage extends SeleniumBase {
	@FindBy(xpath = "//*[@id='nav-logo']/a[1]/span[1]")
    public static WebElement HOMEPAGETITLE;
 
    public HomePage(WebDriver driver) {
        super(driver);
    }
}
