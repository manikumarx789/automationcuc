package com.selenium.test.stepdefs;

import com.selenium.test.pages.Navigations;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.springframework.beans.factory.annotation.Autowired;

import static com.selenium.test.pages.Navigations.*;

public class StepDefScreenNavigations {

	@Autowired

	public Navigations navigations;

	@When("^I click on DEAL,PAY,SELL,CUSTOMER and click on Back to Home$")
	public void thensupportpageshouldbedisplayed() {
		DEAL.click();
		navigations.assertElementPresent(DEAL, 10);
		PAY.click();
		navigations.assertElementPresent(PAY, 10);
		SELL.click();
		navigations.assertElementPresent(SELL, 10);
		CUSTOMER.click();
		navigations.assertElementPresent(CUSTOMER, 10);
	}

}
